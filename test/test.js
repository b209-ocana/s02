const { getCircleArea, checkIfPassed, getAverage, getSum, getDifference, divCheck, isOddOrEven, reverseString } = require('../src/util.js');
//import the assert statements from our chai
const { expect, assert } = require('chai');

//test case - a condition we are testing
//it(stringExplainsWhatTheTestDoes)
//assert is used to assert condition for the test to pass. If the assertion fails then the test is considered failed.
//describe() is used to create a test suite. A test suite is a group of test cases related to one another or tests the same method, data or function
describe('test_get_area_circle_area', () => {

    it('test_area_of_circle_radius_15_is_706.86', () => {
        let area = getCircleArea(15);
        assert.equal(area,706.86);
    })

    it('test_area_of_circle_radius_300_is_282744', () => {
        let area = getCircleArea(300);
        expect(area).to.equal(282744);
    })

})

describe('test_check_if_passed', () => {
    it('test_25_out_of_30_if_passed', () => {
        let isPassed = checkIfPassed(25,30);
        assert.equal(isPassed,true);
    })

    it('test_30_out_of_50_is_not_passed', () => {
        let isPassed = checkIfPassed(30,50);
        expect(isPassed).to.equal(false);
    })
})

describe('test_get_average', () => {
    it('test_average_of_80_82_84_86_is_86', () => {
        let average = getAverage(80,82,84,86);
        assert.equal(average,83);
    })

    it('test_average_of_70_80_82_84_is_79', () => {
        let average = getAverage(70,80,82,84);
        assert.equal(average,79);
    })
})

describe('test_get_sum', () => {
    it('test_sum_of_15_30_is_45', () => {
        let sum = getSum(15,30);
        assert.equal(sum,45);
    })
    it('test_sum_of_25_50_is_75', () => {
        let sum = getSum(25,50);
        assert.equal(sum,75);
    })
})

describe('test_get_difference', () => {
    it('test_difference_of_70_40_is_30', () => {
        let diff = getDifference(70,40);
        assert.equal(diff,30);
    })
    it('test_difference_of_125_50_is_75', () => {
        let diff = getDifference(125,50);
        assert.equal(diff,75);
    })
})

describe('test_div_check', () => {
    it('test_div_50_if_check', () => {
        assert.equal(divCheck(50),true);
    })

    it('test_div_21_if_check', () => {
        assert.equal(divCheck(21),true);
    })

    it('test_div_25_if_check', () => {
        assert.equal(divCheck(25),true);
    })

    it('test_div_77_if_check', () => {
        assert.equal(divCheck(77),true);
    })
})

describe('test_is_odd_or_even', () => {
    it('test_23_is_odd', ()=>{
        assert.equal(isOddOrEven(23),'odd');
    })
    
    it('test_129_is_odd', ()=>{
        assert.equal(isOddOrEven(129),'odd');
    })
    
    it('test_4_is_even', ()=>{
        assert.equal(isOddOrEven(4),'even');
    })
    
    it('test_80_is_even', ()=>{
        assert.equal(isOddOrEven(80),'even');
    })
})

describe('test_reverse_string', () => {
    it('test_reverse_test_is_tset', () => {
        assert.equal(reverseString('test'),'tset');
    })
    
    it('test_reverse_hello_is_olleh', () => {
        assert.equal(reverseString('hello'),'olleh');
    })
    
    it('test_reverse_chaz_is_zahc', () => {
        assert.equal(reverseString('chaz'),'zahc');
    })
    
    it('test_reverse_string_is_gnirts', () => {
        assert.equal(reverseString('string'),'gnirts');
    })
})